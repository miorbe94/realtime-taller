const socket = io();

socket.on('connect', () => {
  const userName = new URLSearchParams(window.location.search).get('username')
  socket.emit('register', userName);
})

socket.on('disconnect', () => {
  console.log('Se ha desconectado');
});

socket.on('message', (message) => {
  addMessage(message);
})

socket.on('users', (users) => {
  showUsers(users);
})

document.querySelector('#message-form').addEventListener('submit', (e) => {
  e.preventDefault();
  const messageField = document.querySelector('#input-message');
  
  socket.emit('message', { message: messageField.value });

  messageField.value = '';
});

function addMessage(message) {
  const messagesContainer = document.querySelector('#messages');
  const messageTemplate = `
  <div class="col-12">
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">${message.user.name}</h5>
        <p>${message.message}</p>
      </div>
    </div>
  </div>
  `;
  messagesContainer.innerHTML += messageTemplate;
}

function showUsers(users) {
  document.querySelector('#users').innerHTML = users.map(el => el.name).join(', ');
}