class Users {
  constructor() {
    this.users = [];
  }

  addUser(user) {
    this.users.push(user);
  }

  getUsers() {
    return this.users;
  }

  getUser(id) {
    const user = this.users.find(user => user.id === id);
    return user;
  }

  deleteUser(id) {
    this.users = this.users.filter(user => user.id !== id);
    return this.users;
  }
}

module.exports = Users;
