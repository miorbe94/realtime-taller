const mongoose = require('mongoose');

const Message = mongoose.model('Message', {
  userName: String,
  message: String
});

module.exports = Message;
