const Users = require('./users');
const Message = require('./models/message');

let io;
const users = new Users();

function setServer(server) {
  io = require('socket.io')(server);

  io.on('connection', (client) => {
    console.log('Se conectó un cliente');

    client.on('disconnect', () => {
      const userArray = users.deleteUser(client.id);
      io.emit('users', userArray);
    });

    client.on('register', userName => {
      users.addUser({
        id: client.id,
        name: userName
      });
      io.emit('users', users.getUsers());
    });

    client.on('message', async ({ message }) => {
      const user = users.getUser(client.id);
      const messageObj = new Message({ userName: user.name, message });
      await messageObj.save();
      io.emit('message', { message, user });
    })

  });
}

module.exports = {
  io,
  setServer
}
