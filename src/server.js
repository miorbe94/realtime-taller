const http = require('http');
const path = require('path');

const express = require('express');
const morgan = require('morgan');

const router = require('./router');
const io = require('./socket');

const app = express();

const server = http.createServer(app);

io.setServer(server);

app.set('view engine', 'pug');
app.set('views', path.join(__dirname, 'views'));

// app.use(morgan('dev'));
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(router);

module.exports = server;
