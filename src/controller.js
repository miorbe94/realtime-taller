const Message = require('./models/message');

exports.getAll = async (req, res) => {
  const { username } = req.query;
  if (!username) return res.redirect('/login');

  const messages = await Message.find();

  return res.render('index', { messages });
}

exports.getLogin = (req, res) => {
  return res.render('login');
}
