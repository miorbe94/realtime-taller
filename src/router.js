const { Router } = require('express');

const controller = require('./controller');

const router = Router();

router.get('/', controller.getAll);
router.get('/login', controller.getLogin);

module.exports = router;
